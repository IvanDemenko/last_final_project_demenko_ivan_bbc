Feature: Opportunity to share story about coronavirus
         As a user I want to be able to share my story by BBC

  @test4
  Scenario: Submitting a story without test in "Tell us your story" placeholder
    Given I am on the BBC HomePage
    And   I open a form to share my story
    When  I fill all fields except (Tell us your story) placeholder
    And   I click SubmitButton
    Then  I am still on the same page

  @test5
  Scenario: Submitting a story without test in "Tell us your story" placeholder
    Given I am on the BBC HomePage
    And   I open a form to share my story
    When  I filled all fields except (Email) placeholder
    And   I click SubmitButton
    Then  I am still on the same page

  @test6
  Scenario: Submitting a story without age checkbox
    Given I am on the BBC HomePage
    And   I open a form to share my story
    When  I filled all fields except Age CheckBox
    And   I click SubmitButton
    Then  I am still on the same page