Feature: Correct article on the BBC News page
         As a user I want to be sure that all articles are correct and true
                   I want to be able to make search in the BBC

  @test1
  Scenario: I check the text of head article
    Given I am on the BBC Homepage
    When  I go to news page
    Then  The correct head article is displayed

  @test2
  Scenario: I check the text of secondary articles
    Given I am on the BBC Homepage
    When  I go to news page
    Then  The correct two secondary articles, below and right, are displayed

  @test3
  Scenario: A user checks the text of head article
    Given I am on the BBC Homepage
    When  I go to news page
    And   I check head article and make search by it
    Then  The correct article is shown with the same text