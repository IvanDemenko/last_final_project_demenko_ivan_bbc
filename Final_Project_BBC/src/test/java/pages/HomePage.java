package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    private static final String BBC_URL = "https://www.bbc.com";

    @FindBy(xpath = "//nav//li[@class='orb-nav-newsdotcom']/a")
    private WebElement newsButton;

    @FindBy(xpath = "//input[@id='orb-search-q']")
    private WebElement searchInput;

    @FindBy(xpath = "//button[contains(@id,'orb')]']")
    private WebElement searchButton;

    public HomePage goTo(){
        driver.get(BBC_URL);
        return this;
    }

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnNewsButton() {
        newsButton.click();
    }

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword);
    }

    public void clickOnSearchButton() {
        searchButton.click();
    }
}
