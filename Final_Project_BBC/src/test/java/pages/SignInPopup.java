package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPopup extends BasePage{

    @FindBy(xpath = "//div[@id='sign_in']")
    private WebElement signInPopup;

    @FindBy(xpath = "//button[@class='sign_in-exit']")
    private WebElement signInExitButton;

    public SignInPopup(WebDriver driver) {
        super(driver);
    }

    public void signIn(){
        if(signInPopup.isDisplayed() == true)
            signInExitButton.click();
    }
}
