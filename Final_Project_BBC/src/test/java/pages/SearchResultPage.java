package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends BasePage {

    @FindBy(xpath = "//span[contains(text(),'Fire destroys')]")
    private WebElement nameOfFirstArticle;

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public String getTextOfNameOfFirstArticle() {
        return nameOfFirstArticle.getText();
    }
}
