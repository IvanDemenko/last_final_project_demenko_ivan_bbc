package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CoronavirusStoriesPage extends BasePage {

    @FindBy(xpath = "//a[@href='/news/10725415']")
    private WebElement shareNewsArticle;

    public CoronavirusStoriesPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnShareNewsArticle() {
        shareNewsArticle.click();
    }
}
