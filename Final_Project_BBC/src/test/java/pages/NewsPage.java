package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewsPage extends BasePage {

    @FindBy(xpath = "//h3[contains(text(),'Armenia and')]")
    private WebElement headlineArticle;

    @FindBy(xpath = "//h3[contains(text(),'Fire destroys')]")
    private WebElement secondaryArticleRight;

    @FindBy(xpath = "//h3[contains(text(),'Fire destroys')]")
    private WebElement secondaryArticleBelowHeadlineArticle;

    @FindBy(xpath = "//li[contains(@class,'menuitem-container')]/a[@class='nw-o-link']/span[text()='Coronavirus']")
    private WebElement coronavirusButton;

    public NewsPage(WebDriver driver) {
        super(driver);
    }

    public String getTextOfHeadlineArticle() {
        return headlineArticle.getText();
    }

    public String getTextOfSecondaryArticleRight() {
        return secondaryArticleRight.getText();
    }

    public String getTextOfSecondaryArticleBelowHeadlineArticle() {
        return secondaryArticleBelowHeadlineArticle.getText();
    }

    public void clickOnCoronavirusButton() {
        coronavirusButton.click();
    }
}
