package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShareNewsPage extends BasePage {

    @FindBy(xpath = "//textarea[@placeholder='Tell us your story. ']")
    public WebElement yourStoryPlaceholder;

    @FindBy(xpath = "//input[@placeholder='Name']")
    public WebElement nameInput;

    @FindBy(xpath = "//input[@placeholder='Email address']")
    public WebElement emailInput;

    @FindBy(xpath = "//input[@placeholder='Contact number ']")
    public WebElement contactNumberInput;

    @FindBy(xpath = "//input[@placeholder='Location ']")
    public WebElement locationInput;

    @FindBy(xpath = "//p[contains(text(),'Please')]/parent::div/parent::span/preceding-sibling::input")
    public WebElement notPublishMyNameCheckBox;

    @FindBy(xpath = "//p[contains(text(),'I am ')]/parent::div/parent::span/preceding-sibling::input")
    public WebElement over16YearsOldCheckBox;

    @FindBy(xpath = "//p[contains(text(),'I accept')]/parent::div/parent::span/preceding-sibling::input")
    public WebElement acceptCheckBox;

    @FindBy(xpath = "//button[text()='Submit']")
    private WebElement submitButton;

    public ShareNewsPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnOver16YearsOldCheckBox() {
        over16YearsOldCheckBox.click();
    }

    public void clickOnSubmitButton() {
        submitButton.click();
    }

    public void fillForm(final String story,
                         final String name,
                         final String email,
                         final String number,
                         final String location)
    {
        yourStoryPlaceholder.sendKeys(story);
        nameInput.sendKeys(name);
        emailInput.sendKeys(email);
        contactNumberInput.sendKeys(number);
        locationInput.sendKeys(location);
        notPublishMyNameCheckBox.click();
        over16YearsOldCheckBox.click();
        acceptCheckBox.click();
    }
}
