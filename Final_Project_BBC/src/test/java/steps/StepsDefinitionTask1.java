package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tests.BaseTest;

import static org.testng.Assert.assertEquals;

public class StepsDefinitionTask1 extends BaseTest {

    private int PAGE_LOAD_COMPLETE_SECONDS = 30;
    private int IMPLICIT_WAIT_SECONDS = 30;

    private String EXPECTED_TEXT_OF_HEADLINE_ARTICLE = "Fire destroys Greece's largest migrant camp";
    private String EXPECTED_TEXT_OF_SECONDARY_ARTICLE_RIGHT = "Climate row as Trump visits fire-hit US West Coast";
    private String EXPECTED_TEXT_OF_SECONDARY_ARTICLE_BELOW_HEADLING_ARTICLE = "Will private firms win the race to Venus?";
    private String EXPECTED_TEXT_OF_FIRST_ARTICLE = "Moria migrants: Fire destroys Greece's largest camp on Lesbos";

    @Before
    public void start(){
        testsSetUp();
    }

    @After
    public void finish(){
        tearDown();
    }

    @Given("I am on the BBC Homepage")
    public void iAmOnTheBBCHomepage() {
        getHomePage().goTo();
    }

    @When("I go to news page")
    public void iGoToNewsPage() {
        getHomePage().clickOnNewsButton();
        getBasePage().waitForPageLoadComplete(PAGE_LOAD_COMPLETE_SECONDS);
        getSignInPopup().signIn();
    }

    @Then("The correct head article is displayed")
    public void theCorrectHeadArticleIsDisplayed() {
        getBasePage().implicitWait(IMPLICIT_WAIT_SECONDS);
        assertEquals(getNewsPage().getTextOfHeadlineArticle(), EXPECTED_TEXT_OF_HEADLINE_ARTICLE);
    }

    @Then("The correct two secondary articles, below and right, are displayed")
    public void theCorrectTwoSecondaryArticlesBelowAndRightAreDisplayed() {
        assertEquals(getNewsPage().getTextOfSecondaryArticleRight(), EXPECTED_TEXT_OF_SECONDARY_ARTICLE_RIGHT);
        assertEquals(getNewsPage().getTextOfSecondaryArticleBelowHeadlineArticle(), EXPECTED_TEXT_OF_SECONDARY_ARTICLE_BELOW_HEADLING_ARTICLE);
    }

    @And("I check head article and make search by it")
    public void iCheckHeadArticleAndMakeSearchByIt() {
        getBasePage().implicitWait(IMPLICIT_WAIT_SECONDS);
        getHomePage().searchByKeyword(getNewsPage().getTextOfHeadlineArticle());
        getHomePage().clickOnSearchButton();
        getBasePage().waitForPageLoadComplete(PAGE_LOAD_COMPLETE_SECONDS);
    }

    @Then("The correct article is shown with the same text")
    public void theCorrectArticleIsShownWithTheSameText() {
        assertEquals(getSearchResultPage().getTextOfNameOfFirstArticle(), EXPECTED_TEXT_OF_FIRST_ARTICLE);
    }
}
