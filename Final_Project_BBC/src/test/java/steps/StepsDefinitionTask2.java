package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import tests.BaseTest;

import static org.testng.Assert.assertEquals;

public class StepsDefinitionTask2 extends BaseTest {

    private int PAGE_LOAD_COMPLETE_SECONDS = 30;
    private int IMPLICIT_WAIT_SECONDS = 30;

    private String NAME = "Tom";
    private String EMAIL = "test1234@test1234.com";
    private String CONTACT_NUMBER = "123456789";
    private String LOCATION = "USA";
    private String EXPECTED_SHARE_NEWS_PAGE_URL = "https://www.bbc.com/news/10725415";
    private String YOUR_STORY_TEXT = "Coronavirus";
    private String YOUR_STORY_TEXT_ERROR = "";
    private String EMAIL_ERROR = "";

    @Before
    public void start(){
        testsSetUp();
    }

    @After
    public void finish(){
        tearDown();
    }

    @Given("I am on the BBC HomePage")
    public void iAmOnTheBBCHomePage() {
        getHomePage().goTo();
    }

    @And("I open a form to share my story")
    public void iOpenAFormToShareMyStory() {
        getHomePage().clickOnNewsButton();
        getBasePage().waitForPageLoadComplete(PAGE_LOAD_COMPLETE_SECONDS);
        getSignInPopup().signIn();
        getBasePage().implicitWait(IMPLICIT_WAIT_SECONDS);
        getNewsPage().clickOnCoronavirusButton();
        getBasePage().waitForPageLoadComplete(PAGE_LOAD_COMPLETE_SECONDS);
        getCoronavirusPage().clickOnYourCoronavirusStoriesButton();
        getBasePage().waitForPageLoadComplete(PAGE_LOAD_COMPLETE_SECONDS);
        getCoronavirusStoriesPage().clickOnShareNewsArticle();
        getBasePage().waitForPageLoadComplete(PAGE_LOAD_COMPLETE_SECONDS);
    }

    @When("I fill all fields except \\(Tell us your story) placeholder")
    public void iFillAllFieldsExceptTellUsYourStoryPlaceholder() {
        getShareNewsPage().fillForm(YOUR_STORY_TEXT_ERROR, NAME, EMAIL, CONTACT_NUMBER, LOCATION);
    }

    @And("I click SubmitButton")
    public void iClickSubmitButton() {
        getShareNewsPage().clickOnSubmitButton();
    }

    @When("I filled all fields except \\(Email) placeholder")
    public void iFilledAllFieldsExceptEmailPlaceholder() {
        getShareNewsPage().fillForm(YOUR_STORY_TEXT, NAME, EMAIL_ERROR, CONTACT_NUMBER, LOCATION);
    }

    @When("I filled all fields except Age CheckBox")
    public void iFilledAllFieldsExceptAgeCheckBox() {
        getShareNewsPage().fillForm(YOUR_STORY_TEXT_ERROR, NAME, EMAIL, CONTACT_NUMBER, LOCATION);
        getShareNewsPage().clickOnOver16YearsOldCheckBox();
    }

    @Then("I am still on the same page")
    public void iAmStillOnTheSamePage() {
        assertEquals(getDriver().getCurrentUrl(), EXPECTED_SHARE_NEWS_PAGE_URL);
    }
}
